var hierarchy =
[
    [ "Timber_and_Stone.AAchievement", "class_timber__and___stone_1_1_a_achievement.html", null ],
    [ "Timber_and_Stone.API.AAdjutant", "class_timber__and___stone_1_1_a_p_i_1_1_a_adjutant.html", [
      [ "Timber_and_Stone.AdjutantGrowth", "class_timber__and___stone_1_1_adjutant_growth.html", null ]
    ] ],
    [ "Timber_and_Stone.API.Event.AEvent", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_a_event.html", [
      [ "Timber_and_Stone.API.Event.EventBlock", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_block.html", [
        [ "Timber_and_Stone.API.Event.EventBlockGrow", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_block_grow.html", null ],
        [ "Timber_and_Stone.API.Event.EventBlockSet", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_block_set.html", null ]
      ] ],
      [ "Timber_and_Stone.API.Event.EventBuildStructure", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_build_structure.html", null ],
      [ "Timber_and_Stone.API.Event.EventChunkLoad", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_chunk_load.html", null ],
      [ "Timber_and_Stone.API.Event.EventCraft", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_craft.html", null ],
      [ "Timber_and_Stone.API.Event.EventDesignationClose", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_designation_close.html", null ],
      [ "Timber_and_Stone.API.Event.EventEntityDeath", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_entity_death.html", null ],
      [ "Timber_and_Stone.API.Event.EventEntityFactionChange", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_entity_faction_change.html", null ],
      [ "Timber_and_Stone.API.Event.EventEntityGroupChange", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_entity_group_change.html", null ],
      [ "Timber_and_Stone.API.Event.EventGameLoad", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_game_load.html", null ],
      [ "Timber_and_Stone.API.Event.EventInvasion", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_invasion.html", null ],
      [ "Timber_and_Stone.API.Event.EventLog", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_log.html", null ],
      [ "Timber_and_Stone.API.Event.EventMerchantArrived", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_merchant_arrived.html", null ],
      [ "Timber_and_Stone.API.Event.EventMigrant", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_migrant.html", null ],
      [ "Timber_and_Stone.API.Event.EventMigrantAccept", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_migrant_accept.html", null ],
      [ "Timber_and_Stone.API.Event.EventMigrantControl", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_migrant_control.html", null ],
      [ "Timber_and_Stone.API.Event.EventMigrantDeny", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_migrant_deny.html", null ],
      [ "Timber_and_Stone.API.Event.EventMine", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_mine.html", null ],
      [ "Timber_and_Stone.API.Event.EventSave", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_save.html", null ],
      [ "Timber_and_Stone.API.Event.EventStorageChange", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_storage_change.html", null ],
      [ "Timber_and_Stone.API.Event.EventStructureFactionChange", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_structure_faction_change.html", null ],
      [ "Timber_and_Stone.API.Event.EventTrade", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_trade.html", null ],
      [ "Timber_and_Stone.API.Event.EventUnitSelect", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_unit_select.html", null ],
      [ "Timber_and_Stone.API.Event.EventWorkTools", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_work_tools.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventBuildBlock", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_build_block.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventDisposeDead", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_dispose_dead.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventGatherGrass", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_gather_grass.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventGatherShrub", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_gather_shrub.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventHarvestFarm", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_harvest_farm.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventLootDead", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_loot_dead.html", null ],
      [ "Timber_and_Stone.API.Event.Task.EventWalkBlock", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_task_1_1_event_walk_block.html", null ]
    ] ],
    [ "Timber_and_Stone.Utility.AItemList", "class_timber__and___stone_1_1_utility_1_1_a_item_list.html", [
      [ "Timber_and_Stone.Utility.Item", "class_timber__and___stone_1_1_utility_1_1_item.html", null ],
      [ "Timber_and_Stone.Utility.ItemList", "class_timber__and___stone_1_1_utility_1_1_item_list.html", null ]
    ] ],
    [ "AManager< AchievementManager >", "class_a_manager.html", [
      [ "Timber_and_Stone.AchievementManager", "class_timber__and___stone_1_1_achievement_manager.html", null ]
    ] ],
    [ "AManager< AssetManager >", "class_a_manager.html", [
      [ "AssetManager", "class_asset_manager.html", null ]
    ] ],
    [ "AManager< ChunkManager >", "class_a_manager.html", [
      [ "ChunkManager", "class_chunk_manager.html", null ],
      [ "ChunkManager", "class_chunk_manager.html", null ]
    ] ],
    [ "AManager< CraftingManager >", "class_a_manager.html", [
      [ "CraftingManager", "class_crafting_manager.html", null ]
    ] ],
    [ "AManager< DesignationManager >", "class_a_manager.html", [
      [ "Timber_and_Stone.DesignationManager", "class_timber__and___stone_1_1_designation_manager.html", null ]
    ] ],
    [ "AManager< DesignManager >", "class_a_manager.html", [
      [ "DesignManager", "class_design_manager.html", null ]
    ] ],
    [ "AManager< GUIManager >", "class_a_manager.html", [
      [ "GUIManager", "class_g_u_i_manager.html", null ]
    ] ],
    [ "AManager< MapManager >", "class_a_manager.html", [
      [ "MapManager", "class_map_manager.html", null ]
    ] ],
    [ "AManager< MusicManager >", "class_a_manager.html", [
      [ "MusicManager", "class_music_manager.html", null ]
    ] ],
    [ "AManager< Options >", "class_a_manager.html", [
      [ "Options", "class_options.html", null ]
    ] ],
    [ "AManager< PluginManager >", "class_a_manager.html", [
      [ "PluginManager", "class_plugin_manager.html", null ]
    ] ],
    [ "AManager< ResourceManager >", "class_a_manager.html", [
      [ "ResourceManager", "class_resource_manager.html", null ]
    ] ],
    [ "AManager< TerrainObjectManager >", "class_a_manager.html", [
      [ "TerrainObjectManager", "class_terrain_object_manager.html", null ]
    ] ],
    [ "AManager< TextureManager >", "class_a_manager.html", [
      [ "TextureManager", "class_texture_manager.html", null ]
    ] ],
    [ "AManager< TimeManager >", "class_a_manager.html", [
      [ "TimeManager", "class_time_manager.html", null ]
    ] ],
    [ "AManager< UnitManager >", "class_a_manager.html", [
      [ "UnitManager", "class_unit_manager.html", null ]
    ] ],
    [ "AManager< WaterManager >", "class_a_manager.html", [
      [ "WaterManager", "class_water_manager.html", null ]
    ] ],
    [ "AManager< WorldManager >", "class_a_manager.html", [
      [ "WorldManager", "class_world_manager.html", null ],
      [ "WorldManager", "class_world_manager.html", null ]
    ] ],
    [ "Timber_and_Stone.Pathfinder.APathfind", "class_timber__and___stone_1_1_pathfinder_1_1_a_pathfind.html", [
      [ "Timber_and_Stone.Pathfinder.Floodfind", "class_timber__and___stone_1_1_pathfinder_1_1_floodfind.html", null ],
      [ "Timber_and_Stone.Pathfinder.Pathfind", "class_timber__and___stone_1_1_pathfinder_1_1_pathfind.html", null ]
    ] ],
    [ "AProfession", null, [
      [ "Timber_and_Stone.AProfession< T >", "class_timber__and___stone_1_1_a_profession.html", [
        [ "Timber_and_Stone.AProfessionCrafter< T >", "class_timber__and___stone_1_1_a_profession_crafter.html", null ]
      ] ]
    ] ],
    [ "Timber_and_Stone.AProfessionCrafter< HumanEntity >", "class_timber__and___stone_1_1_a_profession_crafter.html", [
      [ "Timber_and_Stone.Profession.Human.Blacksmith", "class_timber__and___stone_1_1_profession_1_1_human_1_1_blacksmith.html", null ],
      [ "Timber_and_Stone.Profession.Human.Carpenter", "class_timber__and___stone_1_1_profession_1_1_human_1_1_carpenter.html", null ],
      [ "Timber_and_Stone.Profession.Human.Engineer", "class_timber__and___stone_1_1_profession_1_1_human_1_1_engineer.html", null ],
      [ "Timber_and_Stone.Profession.Human.StoneMason", "class_timber__and___stone_1_1_profession_1_1_human_1_1_stone_mason.html", null ],
      [ "Timber_and_Stone.Profession.Human.Tailor", "class_timber__and___stone_1_1_profession_1_1_human_1_1_tailor.html", null ]
    ] ],
    [ "Timber_and_Stone.Texture.ATextureGroup", "class_timber__and___stone_1_1_texture_1_1_a_texture_group.html", [
      [ "Timber_and_Stone.Texture.ATransitionTextureGroup", "class_timber__and___stone_1_1_texture_1_1_a_transition_texture_group.html", [
        [ "Timber_and_Stone.Texture.TextureGroupTest", "class_timber__and___stone_1_1_texture_1_1_texture_group_test.html", null ],
        [ "Timber_and_Stone.Texture.TextureGroupTransitionBasic", "class_timber__and___stone_1_1_texture_1_1_texture_group_transition_basic.html", null ],
        [ "Timber_and_Stone.Texture.TextureGroupTransitionStone", "class_timber__and___stone_1_1_texture_1_1_texture_group_transition_stone.html", null ]
      ] ],
      [ "Timber_and_Stone.Texture.TBTextureGroup", "class_timber__and___stone_1_1_texture_1_1_t_b_texture_group.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupBrick", "class_timber__and___stone_1_1_texture_1_1_texture_group_brick.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupCrop", "class_timber__and___stone_1_1_texture_1_1_texture_group_crop.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupFieldStone", "class_timber__and___stone_1_1_texture_1_1_texture_group_field_stone.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupRoof", "class_timber__and___stone_1_1_texture_1_1_texture_group_roof.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupScaffolding", "class_timber__and___stone_1_1_texture_1_1_texture_group_scaffolding.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupStone", "class_timber__and___stone_1_1_texture_1_1_texture_group_stone.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupTest2", "class_timber__and___stone_1_1_texture_1_1_texture_group_test2.html", null ],
      [ "Timber_and_Stone.Texture.TextureGroupWeeds", "class_timber__and___stone_1_1_texture_1_1_texture_group_weeds.html", null ]
    ] ],
    [ "System.Attribute", null, [
      [ "Timber_and_Stone.API.Event.EventHandler", "class_timber__and___stone_1_1_a_p_i_1_1_event_1_1_event_handler.html", null ]
    ] ],
    [ "ChunkData.BlockData", "struct_chunk_data_1_1_block_data.html", null ],
    [ "Timber_and_Stone.Blocks.BlockProperties.BlockGroup", "class_timber__and___stone_1_1_blocks_1_1_block_properties_1_1_block_group.html", null ],
    [ "Timber_and_Stone.Collections.BlockingQueue< T >", "class_timber__and___stone_1_1_collections_1_1_blocking_queue.html", null ],
    [ "Timber_and_Stone.Collections.BlockingQueue< Timber_and_Stone.Pathfinder.APathfind >", "class_timber__and___stone_1_1_collections_1_1_blocking_queue.html", null ],
    [ "Timber_and_Stone.BlockPreviewRenderer", "class_timber__and___stone_1_1_block_preview_renderer.html", null ],
    [ "Timber_and_Stone.Blocks.BlockProperties", "class_timber__and___stone_1_1_blocks_1_1_block_properties.html", [
      [ "Timber_and_Stone.Blocks.ASimpleSolidBlock", "class_timber__and___stone_1_1_blocks_1_1_a_simple_solid_block.html", [
        [ "Timber_and_Stone.Blocks.ABlockMineTrack", "class_timber__and___stone_1_1_blocks_1_1_a_block_mine_track.html", [
          [ "Timber_and_Stone.Blocks.BlockMineTrackGrass", "class_timber__and___stone_1_1_blocks_1_1_block_mine_track_grass.html", null ],
          [ "Timber_and_Stone.Blocks.BlockMineTrackStone", "class_timber__and___stone_1_1_blocks_1_1_block_mine_track_stone.html", null ]
        ] ],
        [ "Timber_and_Stone.Blocks.BlockStone2", "class_timber__and___stone_1_1_blocks_1_1_block_stone2.html", null ]
      ] ],
      [ "Timber_and_Stone.Blocks.ASlopeBlock", "class_timber__and___stone_1_1_blocks_1_1_a_slope_block.html", [
        [ "Timber_and_Stone.Blocks.ARoofSlope", "class_timber__and___stone_1_1_blocks_1_1_a_roof_slope.html", [
          [ "Timber_and_Stone.Blocks.SlopeCeramic", "class_timber__and___stone_1_1_blocks_1_1_slope_ceramic.html", null ],
          [ "Timber_and_Stone.Blocks.SlopeCeramic2", "class_timber__and___stone_1_1_blocks_1_1_slope_ceramic2.html", null ],
          [ "Timber_and_Stone.Blocks.SlopeNordicShingles", "class_timber__and___stone_1_1_blocks_1_1_slope_nordic_shingles.html", null ],
          [ "Timber_and_Stone.Blocks.SlopeThatch", "class_timber__and___stone_1_1_blocks_1_1_slope_thatch.html", null ],
          [ "Timber_and_Stone.Blocks.SlopeWoodTile", "class_timber__and___stone_1_1_blocks_1_1_slope_wood_tile.html", null ]
        ] ],
        [ "Timber_and_Stone.Blocks.SlopeDirt", "class_timber__and___stone_1_1_blocks_1_1_slope_dirt.html", null ],
        [ "Timber_and_Stone.Blocks.SlopeGrass", "class_timber__and___stone_1_1_blocks_1_1_slope_grass.html", null ],
        [ "Timber_and_Stone.Blocks.SlopeSand", "class_timber__and___stone_1_1_blocks_1_1_slope_sand.html", null ],
        [ "Timber_and_Stone.Blocks.SlopeStone", "class_timber__and___stone_1_1_blocks_1_1_slope_stone.html", null ]
      ] ],
      [ "Timber_and_Stone.Blocks.ATransitionSolidBlock", "class_timber__and___stone_1_1_blocks_1_1_a_transition_solid_block.html", [
        [ "Timber_and_Stone.Blocks.ABlockOre", "class_timber__and___stone_1_1_blocks_1_1_a_block_ore.html", [
          [ "Timber_and_Stone.Blocks.BlockCoalOre", "class_timber__and___stone_1_1_blocks_1_1_block_coal_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockCopperOre", "class_timber__and___stone_1_1_blocks_1_1_block_copper_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockGoldOre", "class_timber__and___stone_1_1_blocks_1_1_block_gold_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockIronOre", "class_timber__and___stone_1_1_blocks_1_1_block_iron_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockMithrilOre", "class_timber__and___stone_1_1_blocks_1_1_block_mithril_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockSilverOre", "class_timber__and___stone_1_1_blocks_1_1_block_silver_ore.html", null ],
          [ "Timber_and_Stone.Blocks.BlockTinOre", "class_timber__and___stone_1_1_blocks_1_1_block_tin_ore.html", null ]
        ] ],
        [ "Timber_and_Stone.Blocks.BlockBrick1", "class_timber__and___stone_1_1_blocks_1_1_block_brick1.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick2", "class_timber__and___stone_1_1_blocks_1_1_block_brick2.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick3", "class_timber__and___stone_1_1_blocks_1_1_block_brick3.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick4", "class_timber__and___stone_1_1_blocks_1_1_block_brick4.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick5", "class_timber__and___stone_1_1_blocks_1_1_block_brick5.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick6", "class_timber__and___stone_1_1_blocks_1_1_block_brick6.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBrick7", "class_timber__and___stone_1_1_blocks_1_1_block_brick7.html", null ],
        [ "Timber_and_Stone.Blocks.BlockBurnt", "class_timber__and___stone_1_1_blocks_1_1_block_burnt.html", null ],
        [ "Timber_and_Stone.Blocks.BlockCobble0", "class_timber__and___stone_1_1_blocks_1_1_block_cobble0.html", null ],
        [ "Timber_and_Stone.Blocks.BlockCobble1", "class_timber__and___stone_1_1_blocks_1_1_block_cobble1.html", null ],
        [ "Timber_and_Stone.Blocks.BlockDirt0", "class_timber__and___stone_1_1_blocks_1_1_block_dirt0.html", null ],
        [ "Timber_and_Stone.Blocks.BlockFarm", "class_timber__and___stone_1_1_blocks_1_1_block_farm.html", null ],
        [ "Timber_and_Stone.Blocks.BlockFarmNew", "class_timber__and___stone_1_1_blocks_1_1_block_farm_new.html", null ],
        [ "Timber_and_Stone.Blocks.BlockFieldStone", "class_timber__and___stone_1_1_blocks_1_1_block_field_stone.html", null ],
        [ "Timber_and_Stone.Blocks.BlockFlatstone", "class_timber__and___stone_1_1_blocks_1_1_block_flatstone.html", null ],
        [ "Timber_and_Stone.Blocks.BlockFullTimber", "class_timber__and___stone_1_1_blocks_1_1_block_full_timber.html", null ],
        [ "Timber_and_Stone.Blocks.BlockGrass", "class_timber__and___stone_1_1_blocks_1_1_block_grass.html", null ],
        [ "Timber_and_Stone.Blocks.BlockLog", "class_timber__and___stone_1_1_blocks_1_1_block_log.html", null ],
        [ "Timber_and_Stone.Blocks.BlockPavestone0", "class_timber__and___stone_1_1_blocks_1_1_block_pavestone0.html", null ],
        [ "Timber_and_Stone.Blocks.BlockPavestone1", "class_timber__and___stone_1_1_blocks_1_1_block_pavestone1.html", null ],
        [ "Timber_and_Stone.Blocks.BlockPlaster1", "class_timber__and___stone_1_1_blocks_1_1_block_plaster1.html", null ],
        [ "Timber_and_Stone.Blocks.BlockPlaster2", "class_timber__and___stone_1_1_blocks_1_1_block_plaster2.html", null ],
        [ "Timber_and_Stone.Blocks.BlockPlaster3", "class_timber__and___stone_1_1_blocks_1_1_block_plaster3.html", null ],
        [ "Timber_and_Stone.Blocks.BlockSand", "class_timber__and___stone_1_1_blocks_1_1_block_sand.html", null ],
        [ "Timber_and_Stone.Blocks.BlockStone", "class_timber__and___stone_1_1_blocks_1_1_block_stone.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTimberFloor", "class_timber__and___stone_1_1_blocks_1_1_block_timber_floor.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTimberFloor2", "class_timber__and___stone_1_1_blocks_1_1_block_timber_floor2.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTimberFloor3", "class_timber__and___stone_1_1_blocks_1_1_block_timber_floor3.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTimberFloor4", "class_timber__and___stone_1_1_blocks_1_1_block_timber_floor4.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTreeBase", "class_timber__and___stone_1_1_blocks_1_1_block_tree_base.html", null ],
        [ "Timber_and_Stone.Blocks.BlockTreeBaseBurnt", "class_timber__and___stone_1_1_blocks_1_1_block_tree_base_burnt.html", null ],
        [ "Timber_and_Stone.Blocks.BlockWildWheat", "class_timber__and___stone_1_1_blocks_1_1_block_wild_wheat.html", null ],
        [ "Timber_and_Stone.Blocks.BlockWoodLogs", "class_timber__and___stone_1_1_blocks_1_1_block_wood_logs.html", null ]
      ] ],
      [ "Timber_and_Stone.Blocks.BlockAir", "class_timber__and___stone_1_1_blocks_1_1_block_air.html", null ],
      [ "Timber_and_Stone.Blocks.BlockFence", "class_timber__and___stone_1_1_blocks_1_1_block_fence.html", null ],
      [ "Timber_and_Stone.Blocks.BlockScaffolding", "class_timber__and___stone_1_1_blocks_1_1_block_scaffolding.html", [
        [ "Timber_and_Stone.Blocks.BlockScaffoldingTop", "class_timber__and___stone_1_1_blocks_1_1_block_scaffolding_top.html", null ]
      ] ],
      [ "Timber_and_Stone.Blocks.BlockTechnical", "class_timber__and___stone_1_1_blocks_1_1_block_technical.html", [
        [ "Timber_and_Stone.Blocks.BlockTechnicalUnknown", "class_timber__and___stone_1_1_blocks_1_1_block_technical_unknown.html", null ]
      ] ],
      [ "Timber_and_Stone.Blocks.BlockWater", "class_timber__and___stone_1_1_blocks_1_1_block_water.html", null ],
      [ "Timber_and_Stone.Blocks.BlockWaterFlowing", "class_timber__and___stone_1_1_blocks_1_1_block_water_flowing.html", null ]
    ] ],
    [ "Timber_and_Stone.Blocks.BlockRenderCords", "class_timber__and___stone_1_1_blocks_1_1_block_render_cords.html", null ],
    [ "ChunkData", "class_chunk_data.html", null ],
    [ "MithrilAPI.Debug.Cmd.Command", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_command.html", [
      [ "MithrilAPI.Debug.Cmd.ChangeFOV", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_change_f_o_v.html", null ],
      [ "MithrilAPI.Debug.Cmd.ChangeGameMode", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_change_game_mode.html", null ],
      [ "MithrilAPI.Debug.Cmd.GiveResource", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_give_resource.html", null ],
      [ "MithrilAPI.Debug.Cmd.Help", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_help.html", null ],
      [ "MithrilAPI.Debug.Cmd.Save", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_save.html", null ],
      [ "MithrilAPI.Debug.Cmd.SpawnMerchant", "class_mithril_a_p_i_1_1_debug_1_1_cmd_1_1_spawn_merchant.html", null ]
    ] ],
    [ "Timber_and_Stone.Coordinate", "class_timber__and___stone_1_1_coordinate.html", null ],
    [ "CraftingRecipe", "class_crafting_recipe.html", null ],
    [ "EventExecutor< TEvent >IEventInvoker", null, [
      [ "Timber_and_Stone.Event.EventInvoker< TEvent, THandler >", "class_timber__and___stone_1_1_event_1_1_event_invoker.html", null ]
    ] ],
    [ "FarmDesignation.FarmWork", "class_farm_designation_1_1_farm_work.html", null ],
    [ "Timber_and_Stone.GameSave", "class_timber__and___stone_1_1_game_save.html", null ],
    [ "GUIDamageTaken", "class_g_u_i_damage_taken.html", null ],
    [ "Timber_and_Stone.GUIDrawers", "class_timber__and___stone_1_1_g_u_i_drawers.html", [
      [ "Timber_and_Stone.GUIMainMenu", "class_timber__and___stone_1_1_g_u_i_main_menu.html", null ]
    ] ],
    [ "GUITrade", "class_g_u_i_trade.html", null ],
    [ "Timber_and_Stone.API.IBlock", "interface_timber__and___stone_1_1_a_p_i_1_1_i_block.html", null ],
    [ "Timber_and_Stone.API.IBlockData", "interface_timber__and___stone_1_1_a_p_i_1_1_i_block_data.html", [
      [ "Fence", "class_fence.html", null ],
      [ "Timber_and_Stone.API.IBlockDataPersistent", "interface_timber__and___stone_1_1_a_p_i_1_1_i_block_data_persistent.html", null ],
      [ "Timber_and_Stone.API.IBlockDataSave", "interface_timber__and___stone_1_1_a_p_i_1_1_i_block_data_save.html", [
        [ "Timber_and_Stone.BlockData.BlockDataFarm", "class_timber__and___stone_1_1_block_data_1_1_block_data_farm.html", null ],
        [ "Timber_and_Stone.BlockData.BlockDataHealth", "class_timber__and___stone_1_1_block_data_1_1_block_data_health.html", null ],
        [ "Timber_and_Stone.BlockData.BlockDataTextureVariant", "class_timber__and___stone_1_1_block_data_1_1_block_data_texture_variant.html", null ],
        [ "Timber_and_Stone.BlockData.BlockDataVariant", "class_timber__and___stone_1_1_block_data_1_1_block_data_variant.html", null ]
      ] ]
    ] ],
    [ "System.IComparable", null, [
      [ "Timber_and_Stone.AProfession< GoblinEntity >", "class_timber__and___stone_1_1_a_profession.html", [
        [ "Timber_and_Stone.Profession.Goblin.Infantry", "class_timber__and___stone_1_1_profession_1_1_goblin_1_1_infantry.html", null ]
      ] ],
      [ "Timber_and_Stone.AProfession< HumanEntity >", "class_timber__and___stone_1_1_a_profession.html", [
        [ "Timber_and_Stone.Profession.Human.Adventurer", "class_timber__and___stone_1_1_profession_1_1_human_1_1_adventurer.html", null ],
        [ "Timber_and_Stone.Profession.Human.Archer", "class_timber__and___stone_1_1_profession_1_1_human_1_1_archer.html", null ],
        [ "Timber_and_Stone.Profession.Human.Builder", "class_timber__and___stone_1_1_profession_1_1_human_1_1_builder.html", null ],
        [ "Timber_and_Stone.Profession.Human.Farmer", "class_timber__and___stone_1_1_profession_1_1_human_1_1_farmer.html", null ],
        [ "Timber_and_Stone.Profession.Human.Fisherman", "class_timber__and___stone_1_1_profession_1_1_human_1_1_fisherman.html", null ],
        [ "Timber_and_Stone.Profession.Human.Forager", "class_timber__and___stone_1_1_profession_1_1_human_1_1_forager.html", null ],
        [ "Timber_and_Stone.Profession.Human.Herder", "class_timber__and___stone_1_1_profession_1_1_human_1_1_herder.html", null ],
        [ "Timber_and_Stone.Profession.Human.Infantry", "class_timber__and___stone_1_1_profession_1_1_human_1_1_infantry.html", null ],
        [ "Timber_and_Stone.Profession.Human.Merchant", "class_timber__and___stone_1_1_profession_1_1_human_1_1_merchant.html", null ],
        [ "Timber_and_Stone.Profession.Human.Miner", "class_timber__and___stone_1_1_profession_1_1_human_1_1_miner.html", null ],
        [ "Timber_and_Stone.Profession.Human.Trader", "class_timber__and___stone_1_1_profession_1_1_human_1_1_trader.html", null ],
        [ "Timber_and_Stone.Profession.Human.WoodChopper", "class_timber__and___stone_1_1_profession_1_1_human_1_1_wood_chopper.html", null ]
      ] ],
      [ "Timber_and_Stone.AProfession< SkeletonEntity >", "class_timber__and___stone_1_1_a_profession.html", [
        [ "Timber_and_Stone.Profession.Undead.Infantry", "class_timber__and___stone_1_1_profession_1_1_undead_1_1_infantry.html", null ]
      ] ],
      [ "Node", "class_node.html", null ],
      [ "Timber_and_Stone.AProfession< T >", "class_timber__and___stone_1_1_a_profession.html", null ],
      [ "Timber_and_Stone.Pathfinder.Node", "class_timber__and___stone_1_1_pathfinder_1_1_node.html", null ],
      [ "Timber_and_Stone.Texture.ATransitionTextureGroup", "class_timber__and___stone_1_1_texture_1_1_a_transition_texture_group.html", null ]
    ] ],
    [ "IDamageable", "interface_i_damageable.html", [
      [ "ALivingEntity", "class_a_living_entity.html", [
        [ "AAnimalEntity", "class_a_animal_entity.html", [
          [ "Timber_and_Stone.BoarEntity", "class_timber__and___stone_1_1_boar_entity.html", null ],
          [ "Timber_and_Stone.ChickenEntity", "class_timber__and___stone_1_1_chicken_entity.html", null ],
          [ "Timber_and_Stone.SheepEntity", "class_timber__and___stone_1_1_sheep_entity.html", null ]
        ] ],
        [ "APlayableEntity", "class_a_playable_entity.html", [
          [ "HumanEntity", "class_human_entity.html", null ],
          [ "Timber_and_Stone.GoblinEntity", "class_timber__and___stone_1_1_goblin_entity.html", null ],
          [ "Timber_and_Stone.SkeletonEntity", "class_timber__and___stone_1_1_skeleton_entity.html", [
            [ "Timber_and_Stone.NecromancerEntity", "class_timber__and___stone_1_1_necromancer_entity.html", null ]
          ] ]
        ] ],
        [ "Timber_and_Stone.SpiderEntity", "class_timber__and___stone_1_1_spider_entity.html", null ],
        [ "Timber_and_Stone.WolfEntity", "class_timber__and___stone_1_1_wolf_entity.html", null ]
      ] ],
      [ "BuildStructure", "class_build_structure.html", [
        [ "ArcheryTarget", "class_archery_target.html", null ],
        [ "Dummy", "class_dummy.html", null ]
      ] ]
    ] ],
    [ "IEnumerable", null, [
      [ "Timber_and_Stone.Inventory", "class_timber__and___stone_1_1_inventory.html", null ],
      [ "Timber_and_Stone.Inventory", "class_timber__and___stone_1_1_inventory.html", null ]
    ] ],
    [ "Timber_and_Stone.Event.IEventExecutor", "interface_timber__and___stone_1_1_event_1_1_i_event_executor.html", [
      [ "Timber_and_Stone.Event.EventExecutor< TEvent >", "class_timber__and___stone_1_1_event_1_1_event_executor.html", null ]
    ] ],
    [ "Timber_and_Stone.Event.EventExecutor< TEvent >.IEventInvoker", "interface_timber__and___stone_1_1_event_1_1_event_executor_1_1_i_event_invoker.html", null ],
    [ "API.Event.IEventListener", null, [
      [ "DesignManager", "class_design_manager.html", null ]
    ] ],
    [ "Timber_and_Stone.API.Event.IEventListener", "interface_timber__and___stone_1_1_a_p_i_1_1_event_1_1_i_event_listener.html", [
      [ "ControlPlayer", "class_control_player.html", null ],
      [ "ControlPlayer", "class_control_player.html", null ],
      [ "GaiaFactionController", "class_gaia_faction_controller.html", null ],
      [ "GoblinFaction", "class_goblin_faction.html", null ],
      [ "NeutralHostileFaction", "class_neutral_hostile_faction.html", null ],
      [ "PluginManager", "class_plugin_manager.html", null ],
      [ "Timber_and_Stone.AchievementManager", "class_timber__and___stone_1_1_achievement_manager.html", null ],
      [ "Timber_and_Stone.AdjutantGrowth", "class_timber__and___stone_1_1_adjutant_growth.html", null ],
      [ "Timber_and_Stone.Blocks.BlockScaffoldingTop", "class_timber__and___stone_1_1_blocks_1_1_block_scaffolding_top.html", null ],
      [ "Timber_and_Stone.SkeletonEntity", "class_timber__and___stone_1_1_skeleton_entity.html", null ],
      [ "Timber_and_Stone.UnitGroup", "class_timber__and___stone_1_1_unit_group.html", null ],
      [ "Timber_and_Stone.VanillaEventHandler", "class_timber__and___stone_1_1_vanilla_event_handler.html", null ],
      [ "UndeadFaction", "class_undead_faction.html", null ]
    ] ],
    [ "Timber_and_Stone.API.IEventManager", "interface_timber__and___stone_1_1_a_p_i_1_1_i_event_manager.html", [
      [ "Timber_and_Stone.Event.EventManager", "class_timber__and___stone_1_1_event_1_1_event_manager.html", null ]
    ] ],
    [ "IFaction", "interface_i_faction.html", [
      [ "AFaction", "class_a_faction.html", [
        [ "ControlPlayer", "class_control_player.html", null ],
        [ "ControlPlayer", "class_control_player.html", null ],
        [ "MigrantFactionController", "class_migrant_faction_controller.html", null ]
      ] ],
      [ "GaiaFactionController", "class_gaia_faction_controller.html", null ],
      [ "GoblinFaction", "class_goblin_faction.html", null ],
      [ "NeutralHostileFaction", "class_neutral_hostile_faction.html", null ],
      [ "UndeadFaction", "class_undead_faction.html", null ]
    ] ],
    [ "Timber_and_Stone.IGUI", "interface_timber__and___stone_1_1_i_g_u_i.html", [
      [ "Timber_and_Stone.GUIMainMenu", "class_timber__and___stone_1_1_g_u_i_main_menu.html", null ]
    ] ],
    [ "IGUIWindow", "interface_i_g_u_i_window.html", [
      [ "AAnimalEntity", "class_a_animal_entity.html", null ],
      [ "ALivingEntity", "class_a_living_entity.html", null ],
      [ "BuildStructure", "class_build_structure.html", null ],
      [ "FarmDesignation", "class_farm_designation.html", null ],
      [ "GuardPoint", "class_guard_point.html", null ],
      [ "LivestockDesignation", "class_livestock_designation.html", null ],
      [ "PatrolSelector", "class_patrol_selector.html", null ],
      [ "RoadDesignation", "class_road_designation.html", null ],
      [ "Shrub", "class_shrub.html", null ],
      [ "TreeFlora", "class_tree_flora.html", null ]
    ] ],
    [ "Timber_and_Stone.Invasion.IInvasion", "interface_timber__and___stone_1_1_invasion_1_1_i_invasion.html", null ],
    [ "Timber_and_Stone.Invasion.IInvasionGenerator", "interface_timber__and___stone_1_1_invasion_1_1_i_invasion_generator.html", [
      [ "Timber_and_Stone.Invasion.GoblinInvasionGenerator", "class_timber__and___stone_1_1_invasion_1_1_goblin_invasion_generator.html", null ],
      [ "Timber_and_Stone.Invasion.NecromancerInvasionGenerator", "class_timber__and___stone_1_1_invasion_1_1_necromancer_invasion_generator.html", null ],
      [ "Timber_and_Stone.Invasion.SkeletonInvasionGenerator", "class_timber__and___stone_1_1_invasion_1_1_skeleton_invasion_generator.html", null ],
      [ "Timber_and_Stone.Invasion.SpiderInvasionGenerator", "class_timber__and___stone_1_1_invasion_1_1_spider_invasion_generator.html", null ],
      [ "Timber_and_Stone.Invasion.WolfInvasionGenerator", "class_timber__and___stone_1_1_invasion_1_1_wolf_invasion_generator.html", null ]
    ] ],
    [ "IMouseRightClick", "interface_i_mouse_right_click.html", null ],
    [ "Timber_and_Stone.Collections.InsertionSortList< T >", "class_timber__and___stone_1_1_collections_1_1_insertion_sort_list.html", null ],
    [ "Timber_and_Stone.API.IPlugin", "interface_timber__and___stone_1_1_a_p_i_1_1_i_plugin.html", [
      [ "Timber_and_Stone.API.CSharpPlugin", "class_timber__and___stone_1_1_a_p_i_1_1_c_sharp_plugin.html", null ]
    ] ],
    [ "Timber_and_Stone.API.ISaveSection", "interface_timber__and___stone_1_1_a_p_i_1_1_i_save_section.html", [
      [ "ResourceManager", "class_resource_manager.html", null ],
      [ "Timber_and_Stone.AchievementManager", "class_timber__and___stone_1_1_achievement_manager.html", null ]
    ] ],
    [ "Timber_and_Stone.Spell.ISpell", "interface_timber__and___stone_1_1_spell_1_1_i_spell.html", [
      [ "Timber_and_Stone.Spell.Fireball", "class_timber__and___stone_1_1_spell_1_1_fireball.html", null ],
      [ "Timber_and_Stone.Spell.RaiseSkeleton", "class_timber__and___stone_1_1_spell_1_1_raise_skeleton.html", null ]
    ] ],
    [ "Timber_and_Stone.IStorage", "interface_timber__and___stone_1_1_i_storage.html", [
      [ "Timber_and_Stone.IStorageController", "interface_timber__and___stone_1_1_i_storage_controller.html", [
        [ "Timber_and_Stone.ResourceStorage", "class_timber__and___stone_1_1_resource_storage.html", null ],
        [ "Timber_and_Stone.StorageProxy", "class_timber__and___stone_1_1_storage_proxy.html", null ]
      ] ],
      [ "Timber_and_Stone.StorageProxy", "class_timber__and___stone_1_1_storage_proxy.html", null ]
    ] ],
    [ "Timber_and_Stone.Tasks.ITask", "interface_timber__and___stone_1_1_tasks_1_1_i_task.html", [
      [ "Timber_and_Stone.Tasks.ATask", "class_timber__and___stone_1_1_tasks_1_1_a_task.html", [
        [ "Timber_and_Stone.Tasks.ATaskPathfinder", "class_timber__and___stone_1_1_tasks_1_1_a_task_pathfinder.html", [
          [ "Timber_and_Stone.Tasks.TaskGetPathClosestNeighbor", "class_timber__and___stone_1_1_tasks_1_1_task_get_path_closest_neighbor.html", null ],
          [ "Timber_and_Stone.Tasks.TaskGetPathToBlock", "class_timber__and___stone_1_1_tasks_1_1_task_get_path_to_block.html", null ]
        ] ],
        [ "Timber_and_Stone.Tasks.AWorkTask", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html", [
          [ "Timber_and_Stone.Tasks.WorkBuildBlock", "class_timber__and___stone_1_1_tasks_1_1_work_build_block.html", null ],
          [ "Timber_and_Stone.Tasks.WorkBuildStructure", "class_timber__and___stone_1_1_tasks_1_1_work_build_structure.html", null ],
          [ "Timber_and_Stone.Tasks.WorkChopShrub", "class_timber__and___stone_1_1_tasks_1_1_work_chop_shrub.html", null ],
          [ "Timber_and_Stone.Tasks.WorkChopTree", "class_timber__and___stone_1_1_tasks_1_1_work_chop_tree.html", null ],
          [ "Timber_and_Stone.Tasks.WorkCraftResource", "class_timber__and___stone_1_1_tasks_1_1_work_craft_resource.html", null ],
          [ "Timber_and_Stone.Tasks.WorkDisposeLivingEntity", "class_timber__and___stone_1_1_tasks_1_1_work_dispose_living_entity.html", null ],
          [ "Timber_and_Stone.Tasks.WorkExploreChunk", "class_timber__and___stone_1_1_tasks_1_1_work_explore_chunk.html", null ],
          [ "Timber_and_Stone.Tasks.WorkFarmBlock", "class_timber__and___stone_1_1_tasks_1_1_work_farm_block.html", null ],
          [ "Timber_and_Stone.Tasks.WorkFillTrough", "class_timber__and___stone_1_1_tasks_1_1_work_fill_trough.html", null ],
          [ "Timber_and_Stone.Tasks.WorkFish", "class_timber__and___stone_1_1_tasks_1_1_work_fish.html", null ],
          [ "Timber_and_Stone.Tasks.WorkForageShrub", "class_timber__and___stone_1_1_tasks_1_1_work_forage_shrub.html", null ],
          [ "Timber_and_Stone.Tasks.WorkGatherAnimal", "class_timber__and___stone_1_1_tasks_1_1_work_gather_animal.html", null ],
          [ "Timber_and_Stone.Tasks.WorkGatherEnemy", "class_timber__and___stone_1_1_tasks_1_1_work_gather_enemy.html", null ],
          [ "Timber_and_Stone.Tasks.WorkGatherWildWheat", "class_timber__and___stone_1_1_tasks_1_1_work_gather_wild_wheat.html", null ],
          [ "Timber_and_Stone.Tasks.WorkGuardPosition", "class_timber__and___stone_1_1_tasks_1_1_work_guard_position.html", null ],
          [ "Timber_and_Stone.Tasks.WorkHerdAnimal", "class_timber__and___stone_1_1_tasks_1_1_work_herd_animal.html", null ],
          [ "Timber_and_Stone.Tasks.WorkHuntAnimal", "class_timber__and___stone_1_1_tasks_1_1_work_hunt_animal.html", null ],
          [ "Timber_and_Stone.Tasks.WorkLootLivingEntity", "class_timber__and___stone_1_1_tasks_1_1_work_loot_living_entity.html", null ],
          [ "Timber_and_Stone.Tasks.WorkMineBlock", "class_timber__and___stone_1_1_tasks_1_1_work_mine_block.html", null ],
          [ "Timber_and_Stone.Tasks.WorkOperateBallista", "class_timber__and___stone_1_1_tasks_1_1_work_operate_ballista.html", null ],
          [ "Timber_and_Stone.Tasks.WorkPatrolRoute", "class_timber__and___stone_1_1_tasks_1_1_work_patrol_route.html", null ],
          [ "Timber_and_Stone.Tasks.WorkSlaughterAnimal", "class_timber__and___stone_1_1_tasks_1_1_work_slaughter_animal.html", null ],
          [ "Timber_and_Stone.Tasks.WorkTrain", "class_timber__and___stone_1_1_tasks_1_1_work_train.html", null ],
          [ "Timber_and_Stone.Tasks.WorkUpgradeStructure", "class_timber__and___stone_1_1_tasks_1_1_work_upgrade_structure.html", null ]
        ] ],
        [ "Timber_and_Stone.Tasks.TaskAdventure", "class_timber__and___stone_1_1_tasks_1_1_task_adventure.html", null ],
        [ "Timber_and_Stone.Tasks.TaskAdventureAttack", "class_timber__and___stone_1_1_tasks_1_1_task_adventure_attack.html", null ],
        [ "Timber_and_Stone.Tasks.TaskAdventureWalk", "class_timber__and___stone_1_1_tasks_1_1_task_adventure_walk.html", null ],
        [ "Timber_and_Stone.Tasks.TaskAnimalEatFood", "class_timber__and___stone_1_1_tasks_1_1_task_animal_eat_food.html", null ],
        [ "Timber_and_Stone.Tasks.TaskArcher", "class_timber__and___stone_1_1_tasks_1_1_task_archer.html", null ],
        [ "Timber_and_Stone.Tasks.TaskAttack", "class_timber__and___stone_1_1_tasks_1_1_task_attack.html", null ],
        [ "Timber_and_Stone.Tasks.TaskAttackTarget", "class_timber__and___stone_1_1_tasks_1_1_task_attack_target.html", null ],
        [ "Timber_and_Stone.Tasks.TaskCastSpell", "class_timber__and___stone_1_1_tasks_1_1_task_cast_spell.html", null ],
        [ "Timber_and_Stone.Tasks.TaskCheckFreeInvSpace", "class_timber__and___stone_1_1_tasks_1_1_task_check_free_inv_space.html", null ],
        [ "Timber_and_Stone.Tasks.TaskDead", "class_timber__and___stone_1_1_tasks_1_1_task_dead.html", [
          [ "Timber_and_Stone.Tasks.TaskPinned", "class_timber__and___stone_1_1_tasks_1_1_task_pinned.html", null ]
        ] ],
        [ "Timber_and_Stone.Tasks.TaskEatFood", "class_timber__and___stone_1_1_tasks_1_1_task_eat_food.html", null ],
        [ "Timber_and_Stone.Tasks.TaskEmptyInventory", "class_timber__and___stone_1_1_tasks_1_1_task_empty_inventory.html", null ],
        [ "Timber_and_Stone.Tasks.TaskEquipBestHeldArmor", "class_timber__and___stone_1_1_tasks_1_1_task_equip_best_held_armor.html", null ],
        [ "Timber_and_Stone.Tasks.TaskExitMapViaRoads", "class_timber__and___stone_1_1_tasks_1_1_task_exit_map_via_roads.html", null ],
        [ "Timber_and_Stone.Tasks.TaskFeast", "class_timber__and___stone_1_1_tasks_1_1_task_feast.html", null ],
        [ "Timber_and_Stone.Tasks.TaskFindChair", "class_timber__and___stone_1_1_tasks_1_1_task_find_chair.html", null ],
        [ "Timber_and_Stone.Tasks.TaskFlee", "class_timber__and___stone_1_1_tasks_1_1_task_flee.html", null ],
        [ "Timber_and_Stone.Tasks.TaskForcedMove", "class_timber__and___stone_1_1_tasks_1_1_task_forced_move.html", null ],
        [ "Timber_and_Stone.Tasks.TaskGetItems", "class_timber__and___stone_1_1_tasks_1_1_task_get_items.html", null ],
        [ "Timber_and_Stone.Tasks.TaskGroupFollow", "class_timber__and___stone_1_1_tasks_1_1_task_group_follow.html", null ],
        [ "Timber_and_Stone.Tasks.TaskGroupMove", "class_timber__and___stone_1_1_tasks_1_1_task_group_move.html", null ],
        [ "Timber_and_Stone.Tasks.TaskInfantry", "class_timber__and___stone_1_1_tasks_1_1_task_infantry.html", null ],
        [ "Timber_and_Stone.Tasks.TaskLoot", "class_timber__and___stone_1_1_tasks_1_1_task_loot.html", null ],
        [ "Timber_and_Stone.Tasks.TaskMerchant", "class_timber__and___stone_1_1_tasks_1_1_task_merchant.html", null ],
        [ "Timber_and_Stone.Tasks.TaskMigrate", "class_timber__and___stone_1_1_tasks_1_1_task_migrate.html", null ],
        [ "Timber_and_Stone.Tasks.TaskMilitaryEquip", "class_timber__and___stone_1_1_tasks_1_1_task_military_equip.html", null ],
        [ "Timber_and_Stone.Tasks.TaskMoveToHall", "class_timber__and___stone_1_1_tasks_1_1_task_move_to_hall.html", null ],
        [ "Timber_and_Stone.Tasks.TaskRiseFromGround", "class_timber__and___stone_1_1_tasks_1_1_task_rise_from_ground.html", null ],
        [ "Timber_and_Stone.Tasks.TaskSleep", "class_timber__and___stone_1_1_tasks_1_1_task_sleep.html", null ],
        [ "Timber_and_Stone.Tasks.TaskSleep", "class_timber__and___stone_1_1_tasks_1_1_task_sleep.html", null ],
        [ "Timber_and_Stone.Tasks.TaskSleep", "class_timber__and___stone_1_1_tasks_1_1_task_sleep.html", null ],
        [ "Timber_and_Stone.Tasks.TaskSmartWalk< T >", "class_timber__and___stone_1_1_tasks_1_1_task_smart_walk.html", null ],
        [ "Timber_and_Stone.Tasks.TaskSmartWalkMovingTarget", "class_timber__and___stone_1_1_tasks_1_1_task_smart_walk_moving_target.html", null ],
        [ "Timber_and_Stone.Tasks.TaskStopped", "class_timber__and___stone_1_1_tasks_1_1_task_stopped.html", null ],
        [ "Timber_and_Stone.Tasks.TaskTrade", "class_timber__and___stone_1_1_tasks_1_1_task_trade.html", null ],
        [ "Timber_and_Stone.Tasks.TaskWait", "class_timber__and___stone_1_1_tasks_1_1_task_wait.html", null ],
        [ "Timber_and_Stone.Tasks.TaskWalk", "class_timber__and___stone_1_1_tasks_1_1_task_walk.html", null ],
        [ "Timber_and_Stone.Tasks.TaskWalkToStorage", "class_timber__and___stone_1_1_tasks_1_1_task_walk_to_storage.html", null ],
        [ "Timber_and_Stone.Tasks.TaskWander", "class_timber__and___stone_1_1_tasks_1_1_task_wander.html", null ]
      ] ],
      [ "Timber_and_Stone.Tasks.AWorkTask", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html", null ],
      [ "Timber_and_Stone.Tasks.IWorkTask", "interface_timber__and___stone_1_1_tasks_1_1_i_work_task.html", [
        [ "Timber_and_Stone.Tasks.AWorkTask", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html", null ]
      ] ]
    ] ],
    [ "Timber_and_Stone.Utility.UnityThreading.ITask", "interface_timber__and___stone_1_1_utility_1_1_unity_threading_1_1_i_task.html", null ],
    [ "Timber_and_Stone.Texture.IVerticalTransition", "interface_timber__and___stone_1_1_texture_1_1_i_vertical_transition.html", [
      [ "Timber_and_Stone.Texture.TextureGroupRoof", "class_timber__and___stone_1_1_texture_1_1_texture_group_roof.html", null ]
    ] ],
    [ "Timber_and_Stone.Tasks.IWorkPool", "interface_timber__and___stone_1_1_tasks_1_1_i_work_pool.html", [
      [ "FarmDesignation", "class_farm_designation.html", null ],
      [ "Timber_and_Stone.Faction.WorkPools.DisposeDeadWorkpool", "class_timber__and___stone_1_1_faction_1_1_work_pools_1_1_dispose_dead_workpool.html", null ],
      [ "Timber_and_Stone.Faction.WorkPools.ExplorationWorkPool", "class_timber__and___stone_1_1_faction_1_1_work_pools_1_1_exploration_work_pool.html", null ],
      [ "Timber_and_Stone.Faction.WorkPools.LootDeadWorkpool", "class_timber__and___stone_1_1_faction_1_1_work_pools_1_1_loot_dead_workpool.html", null ]
    ] ],
    [ "Timber_and_Stone.Collections.SortedUnrolledLinkedList< T >.LinkedListNode", "class_timber__and___stone_1_1_collections_1_1_sorted_unrolled_linked_list_1_1_linked_list_node.html", null ],
    [ "MapData", "class_map_data.html", null ],
    [ "MonoBehaviour", null, [
      [ "ADesignation", "class_a_designation.html", [
        [ "FarmDesignation", "class_farm_designation.html", null ],
        [ "GenericDesignation", "class_generic_designation.html", null ],
        [ "HallDesignation", "class_hall_designation.html", null ],
        [ "LivestockDesignation", "class_livestock_designation.html", null ],
        [ "RoadDesignation", "class_road_designation.html", null ]
      ] ],
      [ "AFaction", "class_a_faction.html", null ],
      [ "ALivingEntity", "class_a_living_entity.html", null ],
      [ "AManager< T >", "class_a_manager.html", null ],
      [ "AchievementWindow", "class_achievement_window.html", null ],
      [ "Arrow", "class_arrow.html", null ],
      [ "Ballista", "class_ballista.html", null ],
      [ "Billboard", "class_billboard.html", null ],
      [ "BuildStructure", "class_build_structure.html", null ],
      [ "ContextWindow", "class_context_window.html", null ],
      [ "CraftingQueueWindow", "class_crafting_queue_window.html", null ],
      [ "DesignMenu", "class_design_menu.html", null ],
      [ "Door", "class_door.html", null ],
      [ "Equipment", "class_equipment.html", null ],
      [ "FPSCounter", "class_f_p_s_counter.html", null ],
      [ "Fence", "class_fence.html", null ],
      [ "Fire", "class_fire.html", null ],
      [ "FlatLineRenderer", "class_flat_line_renderer.html", null ],
      [ "GuardPoint", "class_guard_point.html", null ],
      [ "HumanSettlerWindow", "class_human_settler_window.html", null ],
      [ "MainMenus", "class_main_menus.html", null ],
      [ "Minetrack", "class_minetrack.html", null ],
      [ "MithrilAPI.Debug.DebugConsole", "class_mithril_a_p_i_1_1_debug_1_1_debug_console.html", null ],
      [ "MithrilAPI.Debug.DebugGUI", "class_mithril_a_p_i_1_1_debug_1_1_debug_g_u_i.html", null ],
      [ "MithrilAPI.Debug.DebugPanel", "class_mithril_a_p_i_1_1_debug_1_1_debug_panel.html", null ],
      [ "ModelAssets", "class_model_assets.html", null ],
      [ "Noise", "class_noise.html", null ],
      [ "NotificationWindow", "class_notification_window.html", null ],
      [ "Occupiable", "class_occupiable.html", null ],
      [ "PatrolPulse", "class_patrol_pulse.html", null ],
      [ "PatrolSelector", "class_patrol_selector.html", null ],
      [ "PostProcess", "class_post_process.html", null ],
      [ "ResourceOLD", "class_resource_o_l_d.html", null ],
      [ "ResourcesMenu", "class_resources_menu.html", null ],
      [ "SSAOEffectDepthCutoff", "class_s_s_a_o_effect_depth_cutoff.html", null ],
      [ "Scaffolding", "class_scaffolding.html", null ],
      [ "Shrub", "class_shrub.html", null ],
      [ "StartSelector", "class_start_selector.html", null ],
      [ "StorageMenu", "class_storage_menu.html", null ],
      [ "Timber_and_Stone.MithrilGUIManager", "class_timber__and___stone_1_1_mithril_g_u_i_manager.html", null ],
      [ "Timber_and_Stone.ModsMenu", "class_timber__and___stone_1_1_mods_menu.html", null ],
      [ "Timber_and_Stone.Utility.GameTime", "class_timber__and___stone_1_1_utility_1_1_game_time.html", null ],
      [ "Timber_and_Stone.Utility.UnityThreading", "class_timber__and___stone_1_1_utility_1_1_unity_threading.html", null ],
      [ "Timber_and_Stone.VSDebug", "class_timber__and___stone_1_1_v_s_debug.html", null ],
      [ "TreeFlora", "class_tree_flora.html", null ],
      [ "TriggerZone", "class_trigger_zone.html", null ],
      [ "TubeRenderer", "class_tube_renderer.html", null ],
      [ "UnitList", "class_unit_list.html", null ],
      [ "WaterChunk", "class_water_chunk.html", null ],
      [ "flickeringLight", "classflickering_light.html", null ]
    ] ],
    [ "Timber_and_Stone.Utility.Tuple.Pair< T1, T2 >", "class_timber__and___stone_1_1_utility_1_1_tuple_1_1_pair.html", null ],
    [ "Timber_and_Stone.Pathfinder", "class_timber__and___stone_1_1_pathfinder.html", null ],
    [ "Timber_and_Stone.PathFinderPath", "class_timber__and___stone_1_1_path_finder_path.html", null ],
    [ "Timber_and_Stone.PatrolPoint", "struct_timber__and___stone_1_1_patrol_point.html", null ],
    [ "Timber_and_Stone.PatrolRoute", "class_timber__and___stone_1_1_patrol_route.html", null ],
    [ "APlayableEntity.Preferences", "class_a_playable_entity_1_1_preferences.html", null ],
    [ "Timber_and_Stone.Utility.Tuple.Quadruple< T1, T2, T3, T4 >", "class_timber__and___stone_1_1_utility_1_1_tuple_1_1_quadruple.html", null ],
    [ "Timber_and_Stone.Utility.Rectangle", "class_timber__and___stone_1_1_utility_1_1_rectangle.html", null ],
    [ "Resource", "class_resource.html", [
      [ "CraftableResource", "class_craftable_resource.html", [
        [ "Armor", "class_armor.html", null ],
        [ "Tool", "class_tool.html", [
          [ "Weapon", "class_weapon.html", null ]
        ] ]
      ] ]
    ] ],
    [ "WorldManager.SaveFileData", "class_world_manager_1_1_save_file_data.html", null ],
    [ "Timber_and_Stone.Utility.Tuple.Single< T1 >", "class_timber__and___stone_1_1_utility_1_1_tuple_1_1_single.html", null ],
    [ "Timber_and_Stone.Collections.SortedUnrolledLinkedList< T >", "class_timber__and___stone_1_1_collections_1_1_sorted_unrolled_linked_list.html", null ],
    [ "Timber_and_Stone.Collections.SortedUnrolledLinkedList< Timber_and_Stone.Pathfinder.Node >", "class_timber__and___stone_1_1_collections_1_1_sorted_unrolled_linked_list.html", null ],
    [ "Timber_and_Stone.StorageType", "struct_timber__and___stone_1_1_storage_type.html", null ],
    [ "Stream", null, [
      [ "Timber_and_Stone.Utility.RunLengthCompressionStream", "class_timber__and___stone_1_1_utility_1_1_run_length_compression_stream.html", null ]
    ] ],
    [ "Timber_and_Stone.Utility.STVector", "struct_timber__and___stone_1_1_utility_1_1_s_t_vector.html", null ],
    [ "Timber_and_Stone.Blocks.BlockRenderCords.Triangle", "class_timber__and___stone_1_1_blocks_1_1_block_render_cords_1_1_triangle.html", null ],
    [ "Timber_and_Stone.Utility.Tuple.Triple< T1, T2, T3 >", "class_timber__and___stone_1_1_utility_1_1_tuple_1_1_triple.html", null ],
    [ "TubeRenderer.TubeVertex", "class_tube_renderer_1_1_tube_vertex.html", null ],
    [ "Timber_and_Stone.Utility.Tuple", "class_timber__and___stone_1_1_utility_1_1_tuple.html", null ],
    [ "Timber_and_Stone.UniqueQueue< T >", "class_timber__and___stone_1_1_unique_queue.html", null ],
    [ "Timber_and_Stone.UniqueQueue< Vector3i >", "class_timber__and___stone_1_1_unique_queue.html", null ],
    [ "Timber_and_Stone.Vector2i", "struct_timber__and___stone_1_1_vector2i.html", null ],
    [ "Timber_and_Stone.Vector3i", "struct_timber__and___stone_1_1_vector3i.html", null ],
    [ "Timber_and_Stone.Blocks.BlockRenderCords.Vertex", "struct_timber__and___stone_1_1_blocks_1_1_block_render_cords_1_1_vertex.html", null ],
    [ "WaterBlock", "class_water_block.html", null ],
    [ "WeakReference", null, [
      [ "Timber_and_Stone.Utility.WeakReference< T >", "class_timber__and___stone_1_1_utility_1_1_weak_reference.html", null ]
    ] ],
    [ "Timber_and_Stone.Tasks.WorkPool", "class_timber__and___stone_1_1_tasks_1_1_work_pool.html", null ]
];