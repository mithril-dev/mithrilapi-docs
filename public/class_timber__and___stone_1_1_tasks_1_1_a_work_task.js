var class_timber__and___stone_1_1_tasks_1_1_a_work_task =
[
    [ "AWorkTask", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a74c7480e55eb44e740123ee2cbb728c8", null ],
    [ "cleanup", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a2b857e3c2412073b5e51dea63c2ad41d", null ],
    [ "getPriority", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#aa25f6649440ea52379d3f6cab9ddca93", null ],
    [ "getPriorityOffset", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#abea1960c21ec5beaa87333fb9a240723", null ],
    [ "isBeingWorked", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a8afce4e725f662ff2857c3b7444a0b86", null ],
    [ "allowBlocklist", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a0d2d98a9451ac6ff1e4817cf4c854096", null ],
    [ "skipFailCounter", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a4318fd57b6f9437a881787fc92d48381", null ],
    [ "unit", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a3d89df7c34c8beb5d92e602c1693f4f4", null ],
    [ "failedAttempts", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#af5adc3d7f2a01e8c09f0d0baaab43b2d", null ],
    [ "workPool", "class_timber__and___stone_1_1_tasks_1_1_a_work_task.html#a84512d8ed623ee1f8b25a2b216b499a8", null ]
];